<?php

function field2attr_attach_fields($entity_type, $entity, $attrs) {
    foreach ($attrs as $key => $value) {
      $attr = is_int($key) ? $value : $key;
      $field_name = $attr == 'body' ? $attr : 'field_' . $attr;
      $field = $entity->$field_name;

      $field_items = field_get_items($entity_type, $entity, $field_name);

      $key_or_callback = is_int($key) ? NULL : $value;

      // $key_or_callback with '#' prefix is the field item key.
      $get_key = strpos($key_or_callback, '#') === 0 ? substr($key_or_callback, 1) : NULL;

      // callback function 
      $callback = (!empty($key_or_callback) && empty($get_key)) ? $key_or_callback : NULL ;

      if (!empty($callback)) {
        $field_value = call_user_func($callback, $field_items[0]);
      }
      else {
        $field_value = field2attr_get_field_value($field_items[0], $get_key);
      }

      $value_name = '_' . $attr;
      $items_name = '_' . $attr . '_items';
      $entity->$value_name = $field_value;
      $entity->$items_name = $field_items;
    }
}

function field2attr_get_field_value($field_item, $get_key = NULL) {
  if ($field_item === NULL) {
    return NULL;
  }

  if (!empty($get_key)) {
    return $field_item[$get_key];
  }

  $possible_item_keys = array('value', 'uri', 'tid', 'fid');

  foreach ($possible_item_keys as $key) {
    if (isset($field_item[$key])) {
      return $field_item[$key];
    }
  }

  // Return the first element value in this field_item.
  return reset($field_item);
}


/**
 * Implements hook_node_load().
 */
function field2attr_node_load($nodes, $types) {
  $defs = module_invoke_all('field2attr_node');

  foreach ($nodes as $nid => $node) {
    if (!isset($defs[$node->type])) {
      continue;
    }

    $attrs = $defs[$node->type];
    field2attr_attach_fields('node', $node, $attrs);
  }
}

// vim: ts=2:sts=2:sw=2
